<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ParticipantRepository")
 * @ORM\HasLifecycleCallbacks
 *
 * @Vich\Uploadable
 */
class Participant
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=50)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $avatar;

    /**
     * @var File
     *
     * @Vich\UploadableField(fileNameProperty="avatar", mapping="participant_avatar")
     */
    private $avatarFile;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="date", nullable=true)
     */
    private $lastParticipatedAt;

    /**
     * @var VacationSlot[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\VacationSlot", mappedBy="participant", cascade={"all"})
     */
    private $vacationSlots;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * @param string $avatar
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;
    }

    /**
     * @return File
     */
    public function getAvatarFile()
    {
        return $this->avatarFile;
    }

    /**
     * @param File $avatarFile
     */
    public function setAvatarFile(File $avatarFile = null)
    {
        $this->avatarFile = $avatarFile;

        if ($avatarFile) {
            $this->updatedAt = new \DateTime;
        }
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return \DateTime
     */
    public function getLastParticipatedAt()
    {
        return $this->lastParticipatedAt;
    }

    /**
     * @param \DateTime $lastParticipatedAt
     */
    public function setLastParticipatedAt($lastParticipatedAt)
    {
        $this->lastParticipatedAt = $lastParticipatedAt;
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->updatedAt = new \DateTime;
    }

    /**
     * @return VacationSlot[]
     */
    public function getVacationSlots()
    {
        return $this->vacationSlots;
    }

    /**
     * @param VacationSlot[] $vacationSlots
     */
    public function setVacationSlots($vacationSlots)
    {
        $this->vacationSlots = $vacationSlots;
    }
}
